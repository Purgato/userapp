package it.aliaslab.usersapp.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import it.aliaslab.usersapp.UserappApplication;
import it.aliaslab.usersapp.model.User;
import it.aliaslab.usersapp.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class UserRestController {
	
	@Autowired
	UserService userService;
	
	public static final Logger logger = LogManager.getLogger(UserappApplication.class);
	
	@GetMapping("/list")
	public List<User> list(){
		logger.info("HTTP GET: /list");
		return userService.listaTot();	
	}

}
