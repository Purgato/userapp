package it.aliaslab.usersapp.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import it.aliaslab.usersapp.model.User;
import it.aliaslab.usersapp.repository.UserRepository;
import it.aliaslab.usersapp.service.UserService;

@Service
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class UserServiceImpl implements UserService {
	
	
	@Autowired
	UserRepository repository;
	
	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Override
	@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
	public List<User> listaTot() {
		List<User> list = (List<User>) repository.findAll();
		logger.debug("E' stata recuperata a lista degli utenti alle ore " + new Date());
		return list;
	}
//	@Override
//	public void insertUsers() {
//		
//		//Lista di utenti test
//		User user1 = new User();
//		User user2 = new User();
//		User user3 = new User();
//		List<User> list = new ArrayList<>();
//		
//		user1.setId(1);
//		user1.setName("Ciccio");
//		user1.setSurname("Franco");
//		user1.setData(new Date());
//		
//		user2.setId(2);
//		user2.setName("Alberto");
//		user2.setSurname("Rossi");
//		user2.setData(new Date());
//		
//		user3.setId(3);
//		user3.setName("Andrea");
//		user3.setSurname("Verdi");
//		user3.setData(new Date());
//		
//		list.add(user1);
//		list.add(user2);
//		list.add(user3);
//		
//		repository.saveAll(list);
//	}
	

	


}
