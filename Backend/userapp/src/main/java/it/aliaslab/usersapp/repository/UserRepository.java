package it.aliaslab.usersapp.repository;

import it.aliaslab.usersapp.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
@Repository
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public interface UserRepository extends CrudRepository<User, Long> {
	
	

}