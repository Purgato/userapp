


drop table if exists user;

CREATE TABLE user (id integer not null,name varchar (1000),surname varchar (1000),birthdate date,primary key(id));

INSERT INTO user (id,name,surname,birthdate)values(1,'Ciccio','Franco','1980-01-01');
INSERT INTO user (id,name,surname,birthdate)values(2,'Alberto','Rossi','1955-01-01');
INSERT INTO user (id,name,surname,birthdate)values(3,'Roberto','Verdi','1967-01-01');