package it.aliaslab.usersapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.aliaslab.usersapp.model.User;
import it.aliaslab.usersapp.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserappApplication.class)
public class SpringBootJPAIntegrationTest {
  
    @Autowired
    private UserRepository repository;
 
    @Test
    public void givenGenericEntityRepository_whenSaveAndRetreiveEntity_thenOK() {
    	User userTest = new User();
    	userTest.setName("Test");
    	userTest.setSurname("Test");
    	User user = repository.save(userTest);
    	User user2 = repository.findOne(user.getId());
        assertNotNull(user);
        assertEquals(user.getName(), user2.getName());
    }
}

