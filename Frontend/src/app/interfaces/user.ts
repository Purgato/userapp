export interface User {
  id: number;
  name: string;
  surname: string;
  birthdate: Date;
}
