import { Injectable } from "@angular/core";
import { User } from "../interfaces/user";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class UserService {
  users: User[] = [];
  private url = '"http://localhost:9999/list"';
  users2: Array<User> = [];

  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get("http://localhost:9999/list");
  }
}
