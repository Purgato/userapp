# UserApp

Web application test in Angular + Springboot

# Start Application

- Dalla cartella Backend/userapp: 

mvn -Dmaven.test.skip=true package

- Lanciare l'applicazione con:

java -jar target/userapp-0.0.1-SNAPSHOT.war


Url: localhost:9999